
$(document).ready(function(){
    // $(window).resize(function(){
    //     setHeight();
    // })
    // $(window).load(function(){
    //     setHeight();
    // })
    $.fn.isInViewport = function() {
        var elementTop = $(this).offset().top;
        var elementBottom = elementTop + $(this).outerHeight();
      
        var viewportTop = $(window).scrollTop();
        var viewportBottom = viewportTop + $(window).height();
      
        return elementBottom > viewportTop && elementTop < viewportBottom;
      };
      
    $("#sticky").sticky({topSpacing:0});
    initSlick();
    initSameHeight();
    vAlign();
    new WOW().init();
    // $(window).on('scroll',function() {
    //     // var stickyTop = $('#site').offset().top;
    //     var stickyTop = $('#registermodal').offset().top;

    //     if ($(window).scrollTop() >= stickyTop) {
    //         if ($('#registermodal').isInViewport()){
    //             if (!Cookies('registerform')) {
    //                 openNav2();
    //             }
    //             $(window).off('scroll');
    //         }
    //     }

    
    // });


    wow2 = new WOW(
        {
        boxClass:     'wow2',      // default
        animateClass: 'animated', // default
        offset:       0,          // default
        mobile:       true,       // default
        live:         true,        // default
        callback: function(el) {

            el.addEventListener('animationend', function () {
            $('.js-typewriter').typewrite({
                speed: 40,
                // fadeIn: false
            });
            });
          }
      }
      )
      wow2.init();

      $('.animatedtext').addClass('animated');
})
function setHeight(){
    var h = $(window).height();
    if($(window).width()>767){
    $('#registermodal, .registerbg, .mastheadcontainer').css('height', h);
    }else{
        $('.mastheadcontainer').css('height', h);
    }
}
function initSlick(){
    $('.homeslider').slick({
        fade: true,
        lazyLoad: 'ondemand',
        arrows: false,
        autoplay: true,
        autoplaySpeed: 5000,
        infinite: true,
        dots: false
    })
    $('.servicesslider').slick({
        dots: true,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        fade: true,
        cssEase: 'linear',
        arrows: false,
        customPaging: function (slider, i) {
            var thumb = $(slider.$slides[i]).data();
            var digit = ("0" + (i+1)).slice(-2);
            return '<span>' + digit + '</span>';
        }
    })
}
function initSameHeight(){
    if(!isXS()){
    $('.sameh').matchHeight();
    }
}
function isXS() {
    return $("#is-xs").css("display") === "block" ? true : false;
  }
function vAlign(inParent) {
    if(!isXS()){
    setTimeout(function () {
        $(".valign").each(function () {
            parentH = !inParent ? $(this).parent().height() / 2 : inParent;
            thsH = $(this).height() / 2;
            $(this).css("margin-top", parentH - thsH + "px");
        });
    }, 200);
  }
  }

  function openNav2() {
    $("#registermodal").css('height', '100%');
    $('body').css('overflow', 'hidden');
  }
  function closeNav2() {
    $("#registermodal").css('height', '0%');
    $('body').css('overflow', 'visible');
  }


$(document).on('click', '#navbar li a', function (e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 80
    }, 500);
});
$(document).on('click', '.masthead a', function (e) {
    e.preventDefault();
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 500);
});


$(document).on('click', '#show1', function (e) {
    e.preventDefault();
    $('.memberlist').hide();
    $('.teamslider').show();
    $('.teamslider').slick(getMemberSettings());
    if(isXS()){
        $('html, body').animate({
            scrollTop: $('.partnerinfo').offset().top
        }, 500);
    }
});

$(document).on('click', '#show2', function (e) {
    e.preventDefault();
    $('.memberlist').hide();
    $('.teamslider').show();
    $('.teamslider').slick(getMemberSettings());
    $('.teamslider').slick('slickGoTo', 1);
    if(isXS()){
        $('html, body').animate({
            scrollTop: $('.partnerinfo').offset().top
        }, 500);
    }
});

function getMemberSettings(){
    return {
        dots: true,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 5000,
        slidesToShow: 1,
        fade: true,
        cssEase: 'linear',
        lazyLoad: 'ondemand',
        arrows: true,
        dots: false,
        mobileFirst: true
    }
  }








jQuery.fn.extend({
    typewrite: function(userArgs){

        var me = this;
        var args = {
            speed: 60,
            callback: false,
            fadeIn: true
        };
        if(typeof userArgs == 'number')
            args.speed = userArgs;
        else {
            args = jQuery.extend(args,userArgs);
        }


        var msg = me.text();
        if( ! msg )
            return false;

        // stop bump
        me.css('position','relative');
        me.html('<span style="z-index:0;opacity:0;">'+msg+'</span>');
        me.css('visibility','visible');
        me.prepend('<span style="z-index:50;position:absolute;" id="typewriter-text"></span>');
        $tt = jQuery('#typewriter-text');

        msg = msg.split('');
        var letter;
        
        var typewriterInterval = window.setInterval(function(){
            if(msg.length) {
                letter = msg.shift();
                
                if(! args.fadeIn) {
                    $tt.append(letter);
                    return;
                }

                letter = '<span class="typewriter-fade-letter" style="display: none;">' + letter + '</span>';
                $tt.append(letter);
                $tt.find('.typewriter-fade-letter').last().fadeIn(300);
            } else {
                window.clearInterval(typewriterInterval);
                if(typeof args.callback == 'function')
                    args.callback();
            }
        },args.speed);
    }
});
